public class FindMin {

    public static void main(String[] args){
        int value1= Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);
        int result1;
        int mainresult;

        boolean someCondition = value1 < value2;
        result1 = someCondition ? value1 : value2;

        boolean someCondition2 = result1 < value3;
        mainresult = someCondition2 ? result1 : value3;

        System.out.println(mainresult);

    }
}
