public class FindGrade {

    public static void main(String[] args){
        int value1= Integer.parseInt(args[0]);

        if (value1 > 100) {
            System.out.println("It's not a valid score!");
        } else if (90 <= value1 & value1 <= 100) {
            System.out.println("A");
        } else if (80 <= value1 & value1 < 90) {
            System.out.println("B");
        } else if (70 <= value1 & value1 < 80) {
            System.out.println("C");
        } else if (60 <= value1 & value1 < 70) {
            System.out.println("D");
        } else if (0 <= value1 & value1 < 60){
            System.out.println("F");
        } else {
            System.out.println("It's not a valid score!");
        }

    }
}
